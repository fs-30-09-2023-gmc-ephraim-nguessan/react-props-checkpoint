import React from 'react';
import { Card } from 'react-bootstrap';

function Player({ nom, equipe, nationalite, jerseyNumber, age, imageUrl }) {
  return (
    <Card style={{ width: '18rem', margin: '10px' }}>
      <Card.Img variant="top" src={imageUrl} alt={nom} />
      <Card.Body>
        <Card.Title>{nom}</Card.Title>
        <Card.Text>
          Equipe: {equipe}
          <br />
          Nationalité: {nationalite}
          <br />
          Numéro de maillot: {jerseyNumber}
          <br />
          Age: {age}
        </Card.Text>
      </Card.Body>
    </Card>
  );
}

export default Player;
