import logo from './logo.svg';
import './App.css';
import React from 'react';
import PlayersList from './PlayersList';

function App() {
  return (
    <div>
      <h1>Liste des joueurs</h1>
      <PlayersList />
    </div>
  );
}

export default App;
