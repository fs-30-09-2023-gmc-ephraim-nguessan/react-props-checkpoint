const players = [
    {
      nom: "Wilfried Zaha",
      equipe: "Galatasaray",
      nationalite: "Ivoirien",
      jerseyNumber: 30,
      age: 34,
      imageUrl: "https://example.com/messi.jpg"
    },
    {
      nom: "Lazare Amani",
      equipe: "",
      nationalite: "Ivoirien",
      jerseyNumber: 7,
      age: 21,
      imageUrl: "https://example.com/ronaldo.jpg"
    },
    {
        nom: "Demar Derozan",
        equipe: "Chicago Bulls",
        nationalite: "Américain",
        jerseyNumber: 24,
        age: 32,
        imageUrl: "https://example.com/ronaldo.jpg"
      },
      {
        nom: "Luka Doncic",
        equipe: "Dallas Mavericks",
        nationalite: "",
        jerseyNumber: 3,
        age: 21,
        imageUrl: "https://example.com/ronaldo.jpg"
      },
    
  ];
  
  export default players;
  